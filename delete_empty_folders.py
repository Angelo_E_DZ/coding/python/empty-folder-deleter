import os

root = input("Enter root folder absolute path: ")

walk = list(os.walk(root))
for path, _, _ in walk[::-1]:
    if len(os.listdir(path)) == 0:
        print("Removing: ", path)
        os.rmdir(path)

input("Done!.\nPress Enter to close...")
